vigtools
=============

#### Make flags


* PREFIX=/usr
* SYSCONFDIR=/etc

#### Dependencies

##### Buildtime:

* make
* shellcheck

##### Runtime:

- base:
  * awk
  * bash
  * coreutils
  * grep
  * pacman
  * util-linux
  * sed

- pkg:
  * vigtools-base
  * binutils
  * diffutils
  * findutils
  * go-yq
  * openssh
  * parallel
  * rsync

- iso:
  * vigtools-base
  * dosfstools
  * e2fsprogs
  * findutils
  * grub
  * libarchive
  * libisoburn
  * mtools
  * squashfs-tools
  * go-yq


#### Configuration

vigtools-{pkg,iso}.conf are the configuration files for vigtools.
By default, the config files are installed in

```bash
/etc/vigtools/vigtools-{pkg,iso}.conf
```

A user vigtools-{pkg,iso}.conf can be placed in

```bash
$HOME/.config/vigtools/vigtools-{pkg,iso}.conf
```

If the userconfig is present, vigtools will load the userconfig values, however, if variables have been set in the systemwide

These values take precedence over the userconfig.
Best practise is to leave systemwide file untouched.
By default it is commented and shows just initialization values done in code.

Tools configuration is done in vigtools-{pkg,iso}.conf or by args.
Specifying args will override vigtools-{pkg,iso}.conf settings.

Both, pacman.conf and makepkg.conf for chroots are loaded from

```bash
usr/share/vigtools/makepkg.conf.d/${arch}.conf
```

```bash
usr/share/vigtools/pacmanconf.d/${repo}-${arch}.conf
```

and can be overridden dropping them in

```bash
$HOME/.config/vigtools/makepkg.conf.d/
```

```bash
$HOME/.config/vigtools/pacman.conf.d/
```

vigtools-*.conf:

```bash
$HOME/.config/vigtools/
```
