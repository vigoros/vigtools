#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${VIGTOOLS_INCLUDE_REPO_IMPORT_SH:-} ]] || return 0
VIGTOOLS_INCLUDE_REPO_IMPORT_SH=1

set -e


patch_pkgbase(){
    local name="$1"
    case "${name}" in
        linux|linux-lts|linux-zen|linux-hardened|linux-rt*)
            sed -e 's|KBUILD_BUILD_HOST=.*|KBUILD_BUILD_HOST=vigoros|' -i PKGBUILD
            sed -e 's|CONFIG_DEFAULT_HOSTNAME=.*|CONFIG_DEFAULT_HOSTNAME="vigoros"|' \
                -i config
        ;;
        *)
            sed -e 's|https://www.archlinux.org/|https://www.artixlinux.org/|' \
                -e 's|(Arch Linux)|(Artix Linux)|' \
                -e 's|arch-meson|vigoros-meson|' \
                -i PKGBUILD
        ;;
    esac
    git --no-pager diff PKGBUILD
}

vigorospkg_repo_import_usage() {
    local -r COMMAND=${_VIGTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS] [PKGBASE]...

    OPTIONS
        --tag TAG          Switch the current workspace to a specified version tag
        --del              Delete files before rsync import
        -h, --help         Show this help text

    EXAMPLES
        $ ${COMMAND} libfoo
        $ ${COMMAND} --tag TAG libfoo
        $ ${COMMAND} --tag TAG --del libfoo
_EOF_
}

vigorospkg_repo_import() {
    if (( $# < 1 )); then
        vigorospkg_repo_import_usage
        exit 0
    fi

    # options
    local pkgbases=()
    local pkgbase
    local TAG
    local rsync_args=()
    rsync_args+=(
        -axcihW
        --no-R
        --no-implied-dirs
        --exclude '.vigoroslinux'
        --exclude '.git'
        --exclude '.gitignore'
        --exclude 'README.md'
        --exclude '*.service'
        --exclude '.SRCINFO'
        --exclude '*.socket'
        --exclude '*.timer'
    )

    while (( $# )); do
        case $1 in
            --tag)
                (( $# <= 1 )) && die "missing argument for %s" "$1"
                TAG="$2"
                shift 2
            ;;
            --tag=*)
                TAG="${1#*=}"
                shift
            ;;
            --del)
                rsync_args+=(--delete-before)
                shift
            ;;
            -h|--help)
                vigorospkg_repo_import_usage
                exit 0
            ;;
            -*)
                die "invalid argument: %s" "$1"
            ;;
            *)
                break
            ;;
        esac
    done

    pkgbases+=("$@")

    for pkgbase in "${pkgbases[@]}"; do

        if [[ -d "${pkgbase}" ]];then

            if [[ ! -d "${pkgbase}/.git" ]]; then
                error "Not a Git repository: ${pkgbase}"
                continue
            fi
            ( cd "${pkgbase}" || return

                stat_busy "Checking for upstream url"
                if ! git config --local --get remote.upstream.url &>/dev/null; then
                    git remote add upstream "${GIT_UPSTREAM_URL}/${pkgbase}".git
                fi
                stat_done

                msg2 "Fetching upstream tags"
                git fetch --tags upstream main

                local latest version
                latest=$(git describe --tags FETCH_HEAD)
                version="${latest}"
                if [[ -n "${TAG}" ]]; then
                    version="${TAG}"
                fi

                if ! has_remote_changes; then
                    msg "Querying ${pkgbase} ..."
                    if ! show_db; then
                        warning "Could not query ${REPO_DB}"
                    fi

                    git checkout "${version}" -b "${version}" &>/dev/null
                    local temp
                    temp=$(mktemp -d --tmpdir "${pkgbase}.XXXXXXXXXX")

                    rsync "${rsync_args[@]}" "$(pwd)"/ "${temp}"/ &>/dev/null
                    git checkout master &>/dev/null
                    git branch -D "${version}" &>/dev/null

                    msg "Importing upstream changeset for ${version}"
                    rsync "${rsync_args[@]}" "${temp}"/ "$(pwd)"/ #&>/dev/null

                    msg2 "Patching ${pkgbase} ..."
                    patch_pkgbase "${pkgbase}"
                fi
            )
        fi

    done
}
