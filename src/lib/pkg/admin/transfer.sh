#!/bin/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${VIGTOOLS_INCLUDE_ADMIN_TRANSFER_SH:-} ]] || return 0
VIGTOOLS_INCLUDE_ADMIN_TRANSFER_SH=1

set -e


vigorospkg_admin_transfer_usage() {
    local -r COMMAND=${_VIGTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS] [PKGBASE]...

    OPTIONS
        -h, --help             Show this help text

    EXAMPLES
        $ ${COMMAND} libfoo
        $ ${COMMAND} libfoo libbar
_EOF_
}



vigorospkg_admin_transfer() {
    if (( $# < 1 )); then
        vigorospkg_admin_transfer_usage
        exit 0
    fi

    # options
    local pkgbases=()
    local pkgbase
    local waste_org="landfill"

    local command=${_VIGTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}

    while (( $# )); do
        case $1 in
            -h|--help)
                vigorospkg_admin_transfer_usage
                exit 0
            ;;
            --)
                shift
                break
            ;;
            -*)
                die "invalid argument: %s" "$1"
            ;;
            *)
                break
            ;;
        esac
    done

    pkgbases+=("$@")

    if [[ -n ${GIT_TOKEN} ]]; then
        for pkgbase in "${pkgbases[@]}"; do
            transfer_repo "${pkgbase}" "${waste_org}"
        done
    fi
}
