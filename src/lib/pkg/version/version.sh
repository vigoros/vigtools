#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${VIGTOOLS_INCLUDE_VERSION_SH:-} ]] || return 0
VIGTOOLS_INCLUDE_VERSION_SH=1

set -e


vigorospkg_version_usage() {
    COMMAND=${_VIGTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS]

    Shows the current version information of vigorospkg

    OPTIONS
        -h, --help    Show this help text
_EOF_
}

vigorospkg_version_print() {
    cat <<- _EOF_
    vigorospkg @buildtoolver@
_EOF_
}

vigorospkg_version() {
    while (( $# )); do
        case $1 in
            -h|--help)
                vigorospkg_version_usage
                exit 0
            ;;
            *)
                die "invalid argument: %s" "$1"
            ;;
        esac
    done

    vigorospkg_version_print
}
