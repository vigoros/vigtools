#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

readonly VIGOROS_DB=(
    system-goblins
    system-gremlins
    system
    world-goblins
    world-gremlins
    world
    asteroids-goblins
    asteroids-gremlins
    asteroids
    lib32-goblins
    lib32-gremlins
    lib32
    galaxy-goblins
    galaxy-gremlins
    galaxy
)

readonly REPO_DB='.vigoroslinux/pkgbase.yaml'

readonly REPO_CI='.vigoroslinux/Jenkinsfile'

yaml_array() {
    local array

    for entry in "$@"; do
        array="${array:-}${array:+,} ${entry}"
    done
    printf "%s\n" "[${array}]"
}

print_package_names() {
    local version
    local architecture pkg
    version=$(get_full_version)
    for pkg in "${pkgname[@]}"; do
        architecture=$(get_pkg_arch "${pkg}")
        printf "%s-%s-%s%s\n" "$pkg" "$version" "$architecture" "$PKGEXT"
    done
}

print_debug_package_names() {
    local version
    local architecture
    version=$(get_full_version)
    if check_option "debug" "y" && check_option "strip" "y"; then
        architecture=$(get_pkg_arch)
        printf "%s-%s-%s-%s%s\n" "$pkgbase" "debug" "$version" "$architecture" "$PKGEXT"
    fi
}

version_from_yaml() {
    local dest="$1"
    local version repo
    repo=".repos.${dest}"
    version=$(repo="${repo}" yq -r 'eval(strenv(repo)).version' "${REPO_DB}")
    printf "%s\n" "${version}"
}

packages_from_yaml() {
    local dest="$1"
    local repo pkgs packages
    repo=".repos.${dest}"
    mapfile -t packages < <(repo="${repo}" yq -r 'eval(strenv(repo)).packages[]' "${REPO_DB}")
    for p in "${packages[@]}"; do
        pkgs="${pkgs:-}${pkgs:+,} ${p}"
    done
    printf "%s\n" "[${pkgs}]"
}

to_bool() {
    local int="$1"
    case "${int}" in
        0) printf "false\n" ;;
        1) printf "true\n" ;;
    esac
}

get_commit_msg() {
    local action="$1"
    local dest="$2"
    local src="$3"
    local version

    case "${action}" in
        add|remove)
            version=$(version_from_yaml "${dest}")
            printf "[%s] '%s' %s\n" "${dest}" "${pkgbase}-${version}" "${action}"
        ;;
        move)
            version=$(version_from_yaml "${src}")
            printf "[%s] -> [%s] '%s' %s\n" "${src}" "${dest}" "${pkgbase}-${version}" "${action}"
        ;;
    esac
}

create_repo_db() {
    [[ -d .vigoroslinux ]] || mkdir .vigoroslinux

    yq -n '"---"' > "${REPO_DB}"

    yq -P 'with(
        .pkgbase;
            .name = null |
            .version = null |
            .arch = [] |
            .pkgname = [] )' \
        -i "${REPO_DB}"

    yq -P 'with(
        .actions;
            .addRepo = null |
            .removeRepo = null |
            .triggersBuild = false |
            .triggersRebuild = false |
            .triggersRepoAdd = false |
            .triggersRepoRemove = false |
            .triggersNoCheck = false )' \
        -i "${REPO_DB}"

    yq -P '.repos = {}' -i "${REPO_DB}"

    for r in "${VIGOROS_DB[@]}"; do
        local repo
        repo=".repos.${r}" \
            yq -P 'with(
                    eval(strenv(repo));
                        .version = null |
                        .packages = [] )' \
                -i "${REPO_DB}"
    done
}

has_repos_map_key() {
    local _r="$1"
    local r
    if ! $(r="$_r" yq -r '.repos | has(strenv(r))' "${REPO_DB}"); then
        return 1
    fi
    return 0
}

# update_repo_db() {
#     local _r
#     for r in asteroids{-goblins,-gremlins,}; do
#         if ! $(_r="$r" yq -r '.repos | has(strenv(_r))' "${REPO_DB}"); then
#             local repo
#             repo=".repos.${r}" \
#                 yq -P 'with(
#                         eval(strenv(repo));
#                             .version = null |
#                             .packages = [] )' \
#                     -i "${REPO_DB}"
#         fi
#     done
# }

update_yaml_base() {
    local version
    local name
    local pkgnames
    local arches

    pkgbase="${pkgbase:-${pkgname}}"
    version="$(get_full_version)"
    pkgnames=$(yaml_array "${pkgname[@]}")
    arches=$(yaml_array "${arch[@]}")

    name="${pkgbase}" version="${version}" pkgnames="${pkgnames}" arches="${arches}" \
        yq -P 'with(
            .pkgbase;
                .name = env(name) |
                .version = env(version) |
                .arch = env(arches) |
                .pkgname = env(pkgnames) )' \
            -i "${REPO_DB}"
}

update_yaml_add() {
    local rebuild="$1"
    local add="$2"
    local nocheck="$3"
    local dest="$4"

    local build=true

    rebuild=$(to_bool "${rebuild}")
    if ${rebuild}; then
        rebuild=true
        build=false
    fi

    add=$(to_bool "${add}")
    if ${add}; then
        local repo
        local pkgs
        local version
        local pkgfiles

        mapfile -t pkgfiles < <(print_package_names)

        pkgs=$(yaml_array "${pkgfiles[@]}")

        version=$(get_full_version)
        repo=".repos.${dest}"

        version="${version}" pkgs="${pkgs}" repo="${repo}" \
            yq -P 'with(
                    eval(strenv(repo));
                        .version = env(version) |
                        .packages = env(pkgs) )' \
                -i "${REPO_DB}"
    fi

    nocheck=$(to_bool "${nocheck}")

    nocheck="${nocheck}" add="${add}" \
    rebuild="${rebuild}" build="${build}" dest="${dest}" \
        yq -P 'with(
            .actions;
                .addRepo = env(dest) |
                .removeRepo = null |
                .triggersBuild = env(build) |
                .triggersRebuild = env(rebuild) |
                .triggersRepoAdd = env(add) |
                .triggersRepoRemove = false |
                .triggersNoCheck = env(nocheck) )' \
            -i "${REPO_DB}"
}

update_yaml_remove() {
    local dest="$1"

    local repo
    repo=".repos.${dest}"

    repo="${repo}" \
        yq -P 'with(
                eval(strenv(repo));
                    .version = null |
                    .packages = [] )' \
            -i "${REPO_DB}"

    dest="${dest}" \
        yq -P 'with(
            .actions;
                .addRepo = null |
                .removeRepo = env(dest) |
                .triggersBuild = false |
                .triggersRebuild = false |
                .triggersRepoAdd = false |
                .triggersRepoRemove = true |
                .triggersNoCheck = false )' \
            -i "${REPO_DB}"
}

update_yaml_move() {
    local src="$1"
    local dest="$2"
    local pkgs
    local version
    local src_repo
    local dest_repo

    src_repo=".repos.${src}"
    dest_repo=".repos.${dest}"

    version=$(version_from_yaml "${src}")
    pkgs=$(packages_from_yaml "${src}")

    src_repo="${src_repo}" \
        yq -P 'with(
                eval(strenv(src_repo));
                    .version = null |
                    .packages = [] )' \
            -i "${REPO_DB}"

    version="${version}" pkgs="${pkgs}" dest_repo="${dest_repo}" \
        yq -P 'with(
                eval(strenv(dest_repo));
                    .version = env(version) |
                    .packages = env(pkgs) )' \
            -i "${REPO_DB}"

    src=${src} dest="${dest}" \
        yq -P 'with(
            .actions;
                .addRepo = env(dest) |
                .removeRepo = env(src) |
                .triggersBuild = false |
                .triggersRebuild = false |
                .triggersRepoAdd = true |
                .triggersRepoRemove = true |
                .triggersNoCheck = false )' \
            -i "${REPO_DB}"
}

show_agent() {
    local agent="orion"
    if grep @galaxy "${REPO_CI}" &>/dev/null; then
        agent="taurus"
    fi
    msg2 "agent: %s" "$agent"
}

show_db() {
    show_agent
    if ! yq -r "${REPO_DB}" 1>/dev/null 2>/dev/null; then
        die "${REPO_DB} invalid!"
    fi
    yq -rP '. | with_entries(select(.value.name))' "${REPO_DB}"
    yq -rP '. | .repos | with_entries(select(.value.version))' "${REPO_DB}"
    return 0
}

show_srcinfo_base() {
    pkg2yaml . | yq '.pkgbase'
}

show_srcinfo_pkgs() {
    pkg2yaml . | yq '.pkgnames'
}
