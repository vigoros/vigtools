#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${VIGTOOLS_INCLUDE_GIT_SH:-} ]] || return 0
VIGTOOLS_INCLUDE_GIT_SH=1

set -e

vigorospkg_git_usage() {
    local -r COMMAND=${_VIGTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [COMMAND] [OPTIONS]

    COMMANDS
        clone          Clone a package repository
        config         Configure a clone according to vigoros specs
        create         Create a new Gitea package repository
        pull           Pull a package repository
        push           Push a package repository

    OPTIONS
        -h, --help     Show this help text

    EXAMPLES
        $ ${COMMAND} clone libfoo linux libbar
        $ ${COMMAND} clone --maintainer tux
        $ ${COMMAND} config --topic mytopic
        $ ${COMMAND} config --maintainer tux
        $ ${COMMAND} create -c libfoo
_EOF_
}

vigorospkg_git() {
    if (( $# < 1 )); then
        vigorospkg_git_usage
        exit 0
    fi

    # option checking
    while (( $# )); do
        case $1 in
        -h|--help)
            vigorospkg_git_usage
            exit 0
        ;;
        clone)
            _VIGTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/git/clone.sh
            source "${LIBDIR}"/pkg/git/clone.sh
            vigorospkg_git_clone "$@"
            exit 0
        ;;
        config)
            _VIGTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/git/config.sh
            source "${LIBDIR}"/pkg/git/config.sh
            vigorospkg_git_config "$@"
            exit 0
        ;;
        create)
            _VIGTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/git/create.sh
            source "${LIBDIR}"/pkg/git/create.sh
            vigorospkg_git_create "$@"
            exit 0
        ;;
        pull)
            _VIGTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/git/pull.sh
            source "${LIBDIR}"/pkg/git/pull.sh
            vigorospkg_git_pull "$@"
            exit 0
        ;;
        push)
            _VIGTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/git/push.sh
            source "${LIBDIR}"/pkg/git/push.sh
            vigorospkg_git_push "$@"
            exit 0
        ;;
        -*)
            die "invalid argument: %s" "$1"
        ;;
        *)
            die "invalid command: %s" "$1"
        ;;
        esac
    done
}
