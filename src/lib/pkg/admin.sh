#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${VIGTOOLS_INCLUDE_ADMIN_SH:-} ]] || return 0
VIGTOOLS_INCLUDE_ADMIN_SH=1

set -e

vigorospkg_admin_usage() {
    local -r COMMAND=${_VIGTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [COMMAND] [OPTIONS]

    COMMANDS
        transfer          Transfer obsolete repository to landfill
        query             Query maintainers and topics
        topic             Manage topics

    OPTIONS
        -h, --help     Show this help text

    EXAMPLES
        $ ${COMMAND} transfer libfoo libbar
        $ ${COMMAND} query --maintainer tux
        $ ${COMMAND} query --topic kf5
_EOF_
}

vigorospkg_admin() {
    if (( $# < 1 )); then
        vigorospkg_admin_usage
        exit 0
    fi

    # option checking
    while (( $# )); do
        case $1 in
        -h|--help)
            vigorospkg_admin_usage
            exit 0
        ;;
        transfer)
            _VIGTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/admin/transfer.sh
            source "${LIBDIR}"/pkg/admin/transfer.sh
            vigorospkg_admin_transfer "$@"
            exit 0
        ;;
        query)
            _VIGTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/admin/query.sh
            source "${LIBDIR}"/pkg/admin/query.sh
            vigorospkg_admin_query "$@"
            exit 0
        ;;
        topic)
            _VIGTOOLS_COMMAND+=" $1"
            shift
            # shellcheck source=src/lib/pkg/admin/query.sh
            source "${LIBDIR}"/pkg/admin/topic.sh
            vigorospkg_admin_topic "$@"
            exit 0
        ;;
        -*)
            die "invalid argument: %s" "$1"
        ;;
        *)
            die "invalid command: %s" "$1"
        ;;
        esac
    done
}
