#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

#{{{ calamares

yaml_array() {
    local array yaml

    for entry in "$@"; do
        yaml="{name: ${entry}, action: enable}"
        array="${array:-}${array:+,} ${yaml}"
    done
    printf "%s\n" "[${array}]"
}

configure_calamares(){
    for config in online offline; do
        local mods="$1/etc/calamares-$config/modules"
        if [[ -d "$mods" ]];then
            msg2 "Configuring: Calamares %s" "$config"

            if [[ -f "$mods"/services-vigoros.conf ]]; then
                local svc
                svc=$(yaml_array "${SERVICES[@]}") \
                    yq -P 'with(.;
                            .command = "vigoros-service" |
                            .services = env(svc) )' \
                        -i "$mods"/services-vigoros.conf
            fi
        fi
    done
    if [[ -d "$1"/etc/calamares-offline ]]; then
        ln -sf calamares-offline "$1"/etc/calamares
    fi
}

#}}}
