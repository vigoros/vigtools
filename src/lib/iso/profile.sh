#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

#{{{ profile

show_profile(){
    msg2 "iso_file: %s" "${iso_file}"
    msg2 "AUTOLOGIN: %s" "${AUTOLOGIN}"
    msg2 "PASSWORD: %s" "${PASSWORD}"
    msg2 "SERVICES: %s" "${SERVICES[*]}"
}

load_profile(){
    local profile_dir="${DATADIR}/iso-profiles"
    [[ -d "${WORKSPACE_DIR}"/iso-profiles ]] && profile_dir="${WORKSPACE_DIR}"/iso-profiles

    root_list="$profile_dir/${profile}/Packages-Root"
    root_overlay="$profile_dir/${profile}/root-overlay"

    [[ -f "$profile_dir/${profile}/Packages-Live" ]] && live_list="$profile_dir/${profile}/Packages-Live"
    [[ -d "$profile_dir/${profile}/live-overlay" ]] && live_overlay="$profile_dir/${profile}/live-overlay"

    common_dir="${DATADIR}/iso-profiles/common"
    [[ -d "$profile_dir"/common ]] && common_dir="${profile_dir}"/common

    [[ -f $profile_dir/${profile}/profile.conf ]] || return 1

    # shellcheck source=contrib/iso/profile.conf.example
    [[ -r "$profile_dir/${profile}"/profile.conf ]] && . "$profile_dir/${profile}"/profile.conf

    AUTOLOGIN=${AUTOLOGIN:-true}

    PASSWORD=${PASSWORD:-'vigoros'}

    if [[ -z "${SERVICES[*]}" ]];then
        SERVICES=('acpid' 'bluetoothd' 'cronie' 'cupsd' 'syslog-ng' 'connmand')
    fi

    return 0
}

read_from_list() {
    local list="$1"
    local _space="s| ||g"
    #local _clean=':a;N;$!ba;s/\n/ /g'
    local _clean='/^$/d'
    local _com_rm="s|#.*||g"
    local _init="s|@initsys@|${INITSYS}|g"

    mapfile -t pkgs < <(sed "$_com_rm" "$list" \
            | sed "$_space" \
            | sed "$_init" \
            | sed "$_clean" | sort -u)
}

load_pkgs(){
    local pkglist="$1"
    packages=()

    if [[ "${pkglist##*/}" == "Packages-Root" ]]; then
        for l in base apps "${INITSYS}"; do
            msg2 "Loading Packages: [%s] ..." "Packages-${l}"
            read_from_list "${common_dir}/Packages-${l}"
            packages+=("${pkgs[@]}")

        done

        if [[ -n "${live_list}" ]]; then
            msg2 "Loading Packages: [Packages-xorg] ..."
            read_from_list "${common_dir}/Packages-xorg"
            packages+=("${pkgs[@]}")
        fi

        for svc in "${SERVICES[@]}"; do
            case "$svc" in
                sddm|gdm|lightdm|mdm|greetd|lxdm|xdm)
                    packages+=("$svc-${INITSYS}"); display_manager="$svc" ;;
                NetworkManager) packages+=("networkmanager-${INITSYS}") ;;
                connmand) packages+=("connman-${INITSYS}") ;;
                cupsd) packages+=("cups-${INITSYS}") ;;
                bluetoothd) packages+=("bluez-${INITSYS}") ;;
                syslog-ng|metalog) packages+=("$svc-${INITSYS}") ;;
            esac
        done

    fi
    msg2 "Loading Packages: [%s] ..." "${pkglist##*/}"
    read_from_list "${pkglist}"
    packages+=("${pkgs[@]}")
}

#}}}
